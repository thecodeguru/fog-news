import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { news } from './news/reducers';
import constants from '../../constants';

const store = createStore(news, constants.initialState, applyMiddleware(thunk));

export default store;