import moment from 'moment';
import axios from '../../../utils/axios';
import { GET_NEWS, GET_NEWS_START, GET_NEWS_ERROR } from './types';
import constants from '../../../constants';
import { NewsUpdater } from '../../../types/News';

const getNews = (data: NewsUpdater) => {
    return {
        type: GET_NEWS,
        data
    }
}

const getNewsStart = (page: number) => {
    return {
        type: GET_NEWS_START,
        page
    }
}

const getNewsError = (error: any) => {
    return {
        type: GET_NEWS_ERROR,
        error
    }
}

export const getAllNews = (page: number, from: any, to: any, q = '') => {
    const params = {
        q: q === '' ? 'apple' : q,
        from: moment(from).format('YYYY-MM-DD'),
        to: moment(to).format('YYYY-MM-DD'),
        sortBy: 'publishedAt',
        apiKey: constants.apiKey,
        pageSize: constants.limit,
        page
    }
    return (dispatch: any) => {
        dispatch(getNewsStart(page))
        return axios.get('everything', { params })
            .then(response => {
                if (response.data.status !== 'error') {
                    dispatch(getNews({ news: response.data, page, q, from, to }))
                }
            })
            .catch(error => {
                dispatch(getNewsError(error))
            });
    };
}