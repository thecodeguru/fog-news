import React from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { getNews } from '../services/news';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import constants from '../constants';
import appHeaderClasses from '../styles/appHeader';
import { ControlState } from '../types/States';

const stateToProps = ({q, dateFrom, dateTo}: ControlState) => {
    return { q, dateFrom, dateTo };
};

const AppHeader = ({ dateFrom, dateTo, q }: ControlState) => {
    const classes = appHeaderClasses();
    let searchValue = constants.initialState.q;

    const [tab, setValue] = React.useState(window.location.pathname);

    const handleChange = (event: React.ChangeEvent<{}>, newValue: string) => {
        setValue(newValue);
    };

    return (
        <div className={classes.root}>
            <AppBar position="fixed">
                <Toolbar>
                    <Typography className={classes.title} variant="h6" noWrap>{constants.appName}</Typography>
                    <div className={classes.search}>
                        <div className={classes.searchIcon}>
                            <SearchIcon />
                        </div>
                        <InputBase
                            onChange={(event) => searchValue = event.target.value}
                            onKeyPress={(ev) => {
                                if (ev.key === 'Enter') {
                                    getNews(1, dateFrom, dateTo, searchValue);
                                    ev.preventDefault();
                                }
                            }}
                            defaultValue={q}
                            placeholder="Search…"
                            classes={{
                                root: classes.inputRoot,
                                input: classes.inputInput,
                            }}
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    </div>
                </Toolbar>
                <Tabs value={tab} onChange={handleChange} aria-label="simple tabs example">
                    <Tab label="News" value="/" component={Link} to="/" />
                    <Tab label="Settings" value="/settings" component={Link} to="/settings" />
                </Tabs>
            </AppBar>
        </div>
    );
}

export default connect(
    stateToProps
)(AppHeader);