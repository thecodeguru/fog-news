import { State } from './States';

export default interface Constants {
    appName: string;
    limit: number;
    apiKey: string,
    initialState: State;   
}