export interface News {
    author: string;
    content: string;
    description: string;
    publishedAt: string;
    source: {
        id: any;
        name: string
    };
    title: string;
    url: string;
    urlToImage: string;
}

export interface NewsUpdater {
    news: News,
    page: number,
    q: string,
    from: string,
    to: string
}

export interface NewsGridProps {
    news: News[],
    isLoading: boolean
}